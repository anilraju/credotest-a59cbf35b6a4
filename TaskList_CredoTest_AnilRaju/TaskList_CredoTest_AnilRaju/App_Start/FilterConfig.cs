﻿using System.Web;
using System.Web.Mvc;

namespace TaskList_CredoTest_AnilRaju
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}