﻿
var localhost = window.location.origin;

var ngApp = angular.module('Tasklist', []);
var onerror = function (data, status, headers, config) { 
    //debugger; 
    alert("error " + status);
};

ngApp.controller('Taskcontroller', function ($scope, $http) {

    var GetTasks = localhost + '/api/task/gettasks';
    var onsuccess_Task = function (data, status, headers, config) {
        //debugger;
        $scope.TaskList = data;
        $scope.divtasklist = true;
    }
    $http.get(GetTasks).success(onsuccess_Task).error(onerror);


    $scope.isCompleted = function (id) {
        //alert(id);
        //debugger;
        var UpdateTask = localhost + '/api/task/UpdateTask';

        var onsuccess_update = function (data, status, headers, config) {
            //debugger;
            alert("Task marked as complete");
            $http.get(GetTasks).success(onsuccess_Task).error(onerror);
        }

        var PostData = { "Id": id };
        $http.put(UpdateTask, PostData).success(onsuccess_update).error(onerror);

    };

    $scope.showaddtask = function()
    {
        $scope.divtasklist = false;
        $scope.divaddtask = true;        
    }

    $scope.showtasklist = function (task) {
        //debugger;
        if (task == undefined) {
            alert("Task cant be blank");
            return false;

        }
        var AddNewTask = localhost + '/api/task/AddNewTask';

        var onsuccess_Insert = function (data, status, headers, config) {
            //debugger;
            alert("New task added sccessfully.");
            $http.get(GetTasks).success(onsuccess_Task).error(onerror);
            $scope.divtasklist = true;
            $scope.divaddtask = false;
        }

        var PostData = { "Description": task };
        $http.post(AddNewTask, PostData).success(onsuccess_Insert).error(onerror);
        //alert(task)       
    }

    $scope.Backtolist = function ()
    {
        $scope.divtasklist = true;
        $scope.divaddtask = false;
    }
});



