﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskList_CredoTest_AnilRaju.WebApi.Model;

namespace TaskList_CredoTest_AnilRaju.WebApi.BusinessLogic
{
    public class DataProcessing
    {
        public List<TaskDisplay> GetTasks(List<Task> tasks)
        {
            List<TaskDisplay> lstobj = new List<TaskDisplay>();
            foreach (var item in tasks)
            {
                TaskDisplay objTask = new TaskDisplay();
                objTask.Id = item.Id;
                objTask.Date = item.EntryDate.ToString("d");
                objTask.Time = item.EntryDate.ToString("HH:mm");
                objTask.Description = item.Description;
                objTask.Completed_Desc = item.IsCompleted == true ? "Completed" : "Not completed";                
                objTask.Completed = item.IsCompleted;

                lstobj.Add(objTask);
            }

            return lstobj;
        }
    }
}