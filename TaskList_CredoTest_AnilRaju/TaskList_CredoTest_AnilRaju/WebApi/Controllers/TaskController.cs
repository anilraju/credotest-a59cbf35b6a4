﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskList_CredoTest_AnilRaju.WebApi.Model;
using TaskList_CredoTest_AnilRaju.WebApi.BusinessLogic;


namespace TaskList_CredoTest_AnilRaju.WebApi
{
    public class TaskController : ApiController
    {
        private TaskListDataContext db = new TaskListDataContext();
               
        [HttpGet]
        [Route("GetTasks")]
        public IHttpActionResult Get()
        {
            var tasks = from t in db.Tasks
                        orderby t.Id
                        descending
                        select t;

            DataProcessing objDataprocessing = new DataProcessing(); 
           
            return Ok(objDataprocessing.GetTasks(tasks.ToList()));
        }       

        [HttpPost]        
        public void AddNewTask(TaskDisplay obj)
        {
            Task newTask = new Task();
            newTask.Description = obj.Description;
            newTask.IsCompleted = false;
            newTask.EntryDate = DateTime.Now;
            db.Tasks.InsertOnSubmit(newTask);
            db.SubmitChanges();
        }
        
        [HttpPut]                
        public void MarkCompleted(TaskDisplay obj)
        {
            var tasks = from t in db.Tasks
                        where t.Id == obj.Id
                        select t;
            foreach (Task match in tasks)
                match.IsCompleted = true;
            db.SubmitChanges();
        }        
    }
}