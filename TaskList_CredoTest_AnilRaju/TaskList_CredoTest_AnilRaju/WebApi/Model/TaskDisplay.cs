﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskList_CredoTest_AnilRaju.WebApi.Model
{
    public class TaskDisplay
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public bool Completed { get; set; }
        public string Completed_Desc { get; set; }
    }
}